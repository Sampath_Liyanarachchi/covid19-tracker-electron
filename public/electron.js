const { app, BrowserWindow, Menu } = require("electron");
const path = require("path");
const isDev = require("electron-is-dev");
const shell = require("electron").shell;

function createWindow() {
  console.log(`file:\\${__dirname}\about.html`);
  // Create the browser window.
  const win = new BrowserWindow({
    width: 1000,
    height: 1000,
    webPreferences: {
      nodeIntegration: true,
    },
  });

  // and load the index.html of the app.
  win.loadURL(
    isDev
      ? "http://localhost:3000"
      : `file://${path.join(__dirname, "../build/index.html")}`
  );

  // Open the DevTools.
  //win.webContents.openDevTools();

  var menu = Menu.buildFromTemplate([
    {
      label: "Menu",
      submenu: [
        {
          label: "About",
          click() {
            let win = new BrowserWindow({
              width: 400,
              height: 200,
              alwaysOnTop: true,
              webPreferences: {
                nodeIntegration: true,
                enableRemoteModule: true,
              },
              frame: false
            });
            win.on("close", function () {
              win = null;
            });
            win.loadURL(
              isDev
                ? `file://${path.join(__dirname, "../public/about.html")}`
                : `file://${path.join(__dirname, "../build/about.html")}`
            );
            win.show();
          },
        },
        { type: "separator" },
        {
          label: "Exit",
          click() {
            app.quit();
          },
        },
      ],
    },
    {
      label: "WorldMeter",
      submenu: [
        {
          label: "Home",
          click() {
            shell.openExternal("https://www.worldometers.info/coronavirus/");
          },
        },
        {
          label: "Sri Lanka",
          click() {
            shell.openExternal(
              "https://www.worldometers.info/coronavirus/country/sri-lanka/"
            );
          },
        },
        {
          label: "Graphs",
          click() {
            shell.openExternal(
              "https://www.worldometers.info/coronavirus/worldwide-graphs/"
            );
          },
        },
      ],
    },
  ]);

  Menu.setApplicationMenu(menu);
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(createWindow);

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
